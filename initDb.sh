#!/bin/sh

H2_JAR="${HOME}/.m2/repository/com/h2database/h2/1.4.190/h2-1.4.190.jar"
JDBC_URL="jdbc:h2:tcp://127.0.1.1:9092/~/.local/var/h2/obs"

_run_script () {
    java -cp ${H2_JAR} org.h2.tools.RunScript -url ${JDBC_URL} -script $1
}

_run_script "./obs-data-model/sql/tables.sql"
_run_script "./obs-data-model/sql/dbuser.sql"
_run_script "./obs-data-model/sql/publishers.sql"
_run_script "./obs-data-model/sql/stocks.sql"
_run_script "./obs-data-model/sql/order.sql"
_run_script "./obs-data-model/sql/recipients.sql"
_run_script "./obs-data-model/sql/order.sql"

