package com.worksap.bootcamp.io;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JDBCTransaction implements Transaction {
	private static final Logger LOG = LoggerFactory.getLogger(JDBCTransaction.class);

	private final Connection connection;

	public JDBCTransaction(final Connection connection) {
		this.connection = Objects.requireNonNull(connection);
	}

	@Override
	public boolean begin() {
		try {
			// Do nothing
			return !connection.isClosed();
		} catch (SQLException e) {
			LOG.error("Can not start transaction.", e);
		}

		return false;
	}

	@Override
	public boolean commit() {
		try {
			connection.commit();
			return true;
		} catch (SQLException e) {
			LOG.error("Can not commit transaction.", e);
		}

		return false;
	}

	@Override
	public boolean rollback() {
		try {
			connection.rollback();
			return true;
		} catch (SQLException e) {
			LOG.error("Can not rollback transaction.", e);
		}

		return false;
	}
}
