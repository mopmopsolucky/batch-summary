package com.worksap.bootcamp.io;

/**
 * This interface wrap any type of transaction.<br>
 * Actually, there is only JDBC transaction, but Service should not touch it.
 * 
 * @author works
 */
public interface Transaction {
	/**
	 * Should call begin before use transaction.
	 * 
	 * @return
	 */
	boolean begin();

	/**
	 * Call commit when you want to commit transaction.
	 * 
	 * @return
	 */
	boolean commit();

	/**
	 * Call rollback when you catch some error or exception, and you want to
	 * cancel your change for DB.
	 * 
	 * @return
	 */
	boolean rollback();
}
