package com.worksap.bootcamp.io;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class allow you to close many times.<br>
 * It designed for share use with many DAOs.
 * 
 * @author works
 */
public final class SharedConnection {
	private final static Logger LOGGER = LoggerFactory.getLogger(SharedConnection.class);

	private final String sid;
	private final String userId;
	private final String password;
	private Transaction transaction;
	private Connection connection = null;
	private boolean isClosed = false;

	public SharedConnection(final String sid, final String userId, final String password) {
		this.sid = Objects.requireNonNull(sid);
		this.userId = Objects.requireNonNull(userId);
		this.password = Objects.requireNonNull(password);
	}

	public void cancel() {
		try {
			getTransaction().rollback();
		} catch (IOException e) {
			LOGGER.error("Rollback transaction throws error.", e);
		}
	}

	public void close() throws SQLException {
		if (isClosed) {
			return;
		}

		try {
			if ((connection != null) && !connection.isClosed()) {
				try {
					getTransaction().commit();
				} catch (IOException e) {
					LOGGER.error("Commit transaction throws error.", e);
				}

				connection.close();
			}
		} finally {
			connection = null;
			transaction = null;
			isClosed = true;
		}
	}

	public Connection getConnection() throws SQLException {
		if (isClosed) {
			throw new SQLException("Connection already closed!");
		}

		if (connection == null) {
			try {
				connection = DriverManager.getConnection(sid, userId, password);
			} catch (SQLException e) {
				connection = null;
				LOGGER.error("Can not get connection.", e);
				throw e;
			}
		}

		return connection;
	}

	public Transaction getTransaction() throws IOException {
		if (transaction == null) {
			try {
				transaction = new JDBCTransaction(getConnection());
			} catch (SQLException e) {
				LOGGER.error("Can not get transaction.", e);
				throw new IOException(e);
			}
		}

		return transaction;
	}
}
