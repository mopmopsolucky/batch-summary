package com.worksap.bootcamp.book;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ISBN {
	private static final Pattern HYPHEN = Pattern.compile("-");

	private ISBN() {
		/* Hide constructor */
	}

	/**
	 * This method test last number(check digit) with modulus 10 weights 3-1. It
	 * is ISBN13 standard.
	 * 
	 * ISBN のチェックディジット（モジュラス10 ウェイト3-1(一括)） 1. チェックディジットを除いた一番左側の桁から順に 1 桁ずつ
	 * 1、3、1、3、1、3…と掛けた数字を作る。 2. 1. で作った 12 個の数字を合計する。 3. 2. で求めた合計値を 10
	 * で割って余りを求める。 4. 3. で求めた余りが 0 なら 0、そうでなければ 10 - (余り) を求める。 4.
	 * の結果の数字がチェックディジットとして ISBN の 13 桁目に書かれているはずです。照らし合わせましょう。 （ISBN が 10
	 * 桁の時には計算方法が違ったため、過去のものを単純に 13 桁に拡張された書籍の場合には一致しません
	 * 今回は新刊の取り込みを想定しているため、一律にエラーとして扱います）
	 * 
	 * 例：ISBN978-4274067976 の場合 1. 9 x 1, 7 x 3, 8 x 1, 4 x 3, 2 x 1, 7 x 3, 4 x
	 * 1, 0 x 3, 6 x 1, 7 x 3, 9 x 1, 7 x 3 2. 9 + 21 + 8 + 12 + 2 + 21 + 4 + 0
	 * + 6 + 21 + 9 + 21 = 134 3. 134 mod 10 = 4 4. 10 - 4 = 6
	 * 
	 * @param originalString
	 * @return
	 */
	public static boolean checkDigit(final String originalString) {
		String target = format(originalString);

		if (target.length() != 13) {
			return false;
		}

		/*
		 * 'X' is ISBN10's check digit.
		 */
		int digit = target.charAt(target.length() - 1) - '0';

		if ((digit >= 10) || (digit < 0)) {
			return false;
		}

		int sum = 0;

		for (int ii = 0; ii < target.length() - 1; ii++) {
			int num = target.charAt(ii) - '0';

			if ((num >= 10) || (num < 0)) {
				return false;
			}

			sum += num * (1 + (ii % 2) * 2);
		}

		int value = sum % 10;

		if (value == 0) {
			return digit == 0;
		}

		return digit == 10 - value;
	}

	/**
	 * This method remove "isbn" prefix and "-". And if it has 'x'. Change to
	 * upper case('X').
	 * 
	 * @param originalString
	 * @return
	 */
	public static String format(final String originalString) {
		String result = originalString.toUpperCase().replace("ISBN", "");
		Matcher matcher = HYPHEN.matcher(result);
		result = matcher.replaceAll("");
		return result;
	}
}
