package com.worksap.bootcamp.batch.summary.lv1;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application entry point
 */
public final class App {
	private final static Logger LOGGER = LoggerFactory.getLogger(App.class);

	App() {
		/* Hide constructor */
	}

	public static void main(final String[] args) {
		Options options = new Options();
		CmdLineParser parser = new CmdLineParser(options);

		try {
			parser.parseArgument(args);

			if (!options.hasDBConnectParameter()) {
				throw new CmdLineException(parser, new IllegalArgumentException("There is no DB connect parameter."));
			}

			App app = new App();
			app.execute(options);
		} catch (CmdLineException e) {
			LOGGER.error("This application request [-y, -m] arguments and [-s, -u, -p] arguments.\nUsage is...\n\t"
					+ parser.printExample(OptionHandlerFilter.ALL) + "\n", e);
			parser.printUsage(System.err);
		} catch (Exception e) {
			LOGGER.error("Summary calcuration throw error!", e);
		}

		// TODO should return result status
	}

	void execute(final Options options) throws Exception {
		// TODO implements
	}
}
