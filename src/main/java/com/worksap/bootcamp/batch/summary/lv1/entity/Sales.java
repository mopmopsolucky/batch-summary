package com.worksap.bootcamp.batch.summary.lv1.entity;

public final class Sales {
	private final String bookTitle;
	private final String isbn;
	private final int bookId;
	private final int bookPrice;
	private final int month;
	private final int publisherId;
	private final int year;
	private final int salesQuantity;

	/**
	 * Entity of sales
	 * 
	 * @param year
	 *            Sold year
	 * @param month
	 *            Sold month
	 * @param bookId
	 *            Book ID.
	 * @param bookTitle
	 *            Book title.
	 * @param publisherId
	 *            Publisher ID.
	 * @param isbn
	 *            ISBN code without '-' and prefix('ISBN').
	 * @param bookPrice
	 *            Book price (How much, Yen).
	 * @param salesQuantity
	 *            Sales quantity (How many copies).
	 */
	public Sales(final int year, final int month, final int bookId, final String bookTitle, final int publisherId,
			final String isbn, final int bookPrice, final int salesQuantity) {
		super();
		this.bookTitle = bookTitle;
		this.isbn = isbn;
		this.bookId = bookId;
		this.bookPrice = bookPrice;
		this.month = month;
		this.publisherId = publisherId;
		this.year = year;
		this.salesQuantity = salesQuantity;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public String getIsbn() {
		return isbn;
	}

	public int getBookId() {
		return bookId;
	}

	public int getBookPrice() {
		return bookPrice;
	}

	public int getMonth() {
		return month;
	}

	public int getPublisherId() {
		return publisherId;
	}

	public int getYear() {
		return year;
	}

	public int getSalesQuantity() {
		return salesQuantity;
	}
}
