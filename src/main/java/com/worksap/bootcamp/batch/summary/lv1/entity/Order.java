package com.worksap.bootcamp.batch.summary.lv1.entity;

public final class Order {
	private final String bookTitle;
	private final String isbn;
	private final int bookId;
	private final int price;
	private final int publisherId;
	private final int salesQuantity;

	public Order(final int bookId, final String bookTitle, final int publisherId, final String isbn, final int price,
			final int salesQuantity) {
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.publisherId = publisherId;
		this.isbn = isbn;
		this.price = price;
		this.salesQuantity = salesQuantity;
	}

	public int getBookId() {
		return bookId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public int getPublisherId() {
		return publisherId;
	}

	public String getIsbn() {
		return isbn;
	}

	public int getPrice() {
		return price;
	}

	public int getSalesQuantity() {
		return salesQuantity;
	}
}
