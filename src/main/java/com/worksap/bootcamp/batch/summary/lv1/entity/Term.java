package com.worksap.bootcamp.batch.summary.lv1.entity;

public final class Term {
	private final int month;
	private final int year;

	public Term(final int year, final int month) {
		this.year = year;
		this.month = month;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}
}
