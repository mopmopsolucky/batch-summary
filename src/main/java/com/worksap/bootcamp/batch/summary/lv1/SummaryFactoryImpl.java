package com.worksap.bootcamp.batch.summary.lv1;

import java.io.IOException;

import com.worksap.bootcamp.batch.summary.lv1.entity.Order;
import com.worksap.bootcamp.batch.summary.lv1.entity.Sales;
import com.worksap.bootcamp.batch.summary.lv1.entity.Term;
import com.worksap.bootcamp.io.Transaction;
import com.worksap.bootcamp.template.RecordReader;
import com.worksap.bootcamp.template.Summarizer;
import com.worksap.bootcamp.template.SummaryFactory;
import com.worksap.bootcamp.template.SummaryWriter;

public final class SummaryFactoryImpl
		implements SummaryFactory<Order, Sales, Term> {
	public SummaryFactoryImpl(final String sid, final String userId,
			final String password, final int year, final int month) {
	}

	@Override
	public RecordReader<Order> getRecordReader() {
		return null;
	}

	@Override
	public Summarizer<Order, Sales> getSummarizer() {
		return null;
	}

	@Override
	public SummaryWriter<Sales, Term> getSummaryWriter() {
		return null;
	}

	@Override
	public Transaction getTransaction() throws IOException {
		return null;
	}
}
