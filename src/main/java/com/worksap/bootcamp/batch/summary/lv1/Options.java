package com.worksap.bootcamp.batch.summary.lv1;

import org.kohsuke.args4j.Option;

/**
 * Parsed Command line Arguments
 */
public final class Options {
	@Option(name = "-m", aliases = "--month", metaVar = "INT", usage = "Month(1-12) of target current year and month", required = true)
	private int month;
	@Option(name = "-y", aliases = "--year", metaVar = "INT", usage = "Year of target current year and month", required = true)
	private int year;
	@Option(name = "-s", aliases = "--sid", metaVar = "STR", usage = "JDBC SID", forbids = { "-f" }, depends = { "-u",
			"-p" })
	private String sid;
	@Option(name = "-u", aliases = "--user", metaVar = "STR", usage = "JDBC user ID", forbids = { "-f" }, depends = {
			"-s", "-p" })
	private String userId;
	@Option(name = "-p", aliases = "--password", metaVar = "STR", usage = "JDBC user's password", forbids = {
			"-f" }, depends = { "-s", "-u" })
	private String password;

	public String getJDBCPassword() {
		if (password != null) {
			return password;
		}

		return password;
	}

	public String getJDBCUserId() {
		if (userId != null) {
			return userId;
		}

		return userId;
	}

	public String getJDBCURL() {
		if (sid != null) {
			return sid;
		}

		return sid;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public boolean hasDBConnectParameter() {
		return sid != null;
	}

	/*
	 * For unit test
	 */
	void setMonth(int month) {
		this.month = month;
	}

	/*
	 * For unit test
	 */
	void setYear(int year) {
		this.year = year;
	}

	/*
	 * For unit test
	 */
	void setSid(String sid) {
		this.sid = sid;
	}

	/*
	 * For unit test
	 */
	void setUserId(String userId) {
		this.userId = userId;
	}

	/*
	 * For unit test
	 */
	void setPassword(String password) {
		this.password = password;
	}
}
