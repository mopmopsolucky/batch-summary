package com.worksap.bootcamp.batch.summary.lv1.writer;

import java.io.IOException;

import com.worksap.bootcamp.batch.summary.lv1.entity.Sales;
import com.worksap.bootcamp.batch.summary.lv1.entity.Term;
import com.worksap.bootcamp.io.SharedConnection;
import com.worksap.bootcamp.template.SummaryWriter;

public class SummaryWriterImpl implements SummaryWriter<Sales, Term> {
	public SummaryWriterImpl(final SharedConnection sharedConnection) {
	}

	@Override
	public void close() throws Exception {
	}

	@Override
	public boolean initialize(final Term condition) throws IOException {
		return false;
	}

	@Override
	public void insert(final Sales summary) throws IOException {
	}
}
