package com.worksap.bootcamp.template;

import java.io.IOException;

public interface RecordWriter<E> extends AutoCloseable {
	void delete(E target) throws IOException;

	void insert(E target) throws IOException;

	void update(E target) throws IOException;
}
