package com.worksap.bootcamp.template;

import java.io.IOException;

import com.worksap.bootcamp.io.Transaction;

/**
 * It will return a group of Classes.
 * 
 * @param <C>
 *            type of CSV Record
 * @param <E>
 *            type of Entity
 * @param <T>
 *            type of Entity's Unique key
 */
public interface ImportFactory<C, E, T> {
	AutoComplete<C> getAutoComplete() throws IOException;

	CSVReader<C> getCSVReader() throws IOException;

	IDManager<T, C> getIDManager() throws IOException;

	Transaction getTransaction() throws IOException;

	RecordWriter<E> getRecordWriter() throws IOException;

	Validator<C> getValidator() throws IOException;
}
