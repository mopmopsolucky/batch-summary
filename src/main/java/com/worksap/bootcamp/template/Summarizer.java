package com.worksap.bootcamp.template;

public interface Summarizer<E, S> {
	/**
	 * After all entity put, you need to call this method and get last summary.
	 * 
	 * @return
	 */
	S lastSummary();

	/**
	 * Make summary with entity.
	 * 
	 * @param entity
	 * @return If entity summarize key is break, return summary. Or if key is
	 *         same, return null.
	 */
	S summarize(E entity);
}
