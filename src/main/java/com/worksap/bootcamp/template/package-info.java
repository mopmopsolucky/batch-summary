/**
 * This package include many interfaces.<br>
 * These interface are for Bootcamp JUnit test.<br>
 * Please implements these interfaces.<br>
 * I mean, if you create original class, we can not test it.<br>
 * 
 * But we do not recommend to use them for product development.<br>
 * Product has own feature, then they will have own interfaces or rules.
 */
package com.worksap.bootcamp.template;
