package com.worksap.bootcamp.template;

public interface AutoComplete<C> {
	/**
	 * To avoid "same instance" issue, you should return new instance of <C>.
	 * (It means, you should treat <C> as immutable).
	 * 
	 * @param input
	 * @return
	 */
	C complete(C input);
}
