package com.worksap.bootcamp.template;

import java.io.IOException;

public interface IDManager<T, C> {
	/**
	 * 
	 * @param csvRecord
	 * @return null if not found.
	 * @throws IOException
	 */
	T findID(C csvRecord) throws IOException;

	/**
	 * 
	 * @param csvRecord
	 * @return New ID or Exception if can not generate.
	 * @throws IOException
	 */
	T generateID(C csvRecord) throws IOException;
}
