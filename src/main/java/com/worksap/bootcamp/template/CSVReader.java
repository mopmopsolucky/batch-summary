package com.worksap.bootcamp.template;

public interface CSVReader<E> extends AutoCloseable {
	/**
	 * You should return next record or null(No data or end of data)
	 * 
	 * @return Record
	 */
	E next();
}
