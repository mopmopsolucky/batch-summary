package com.worksap.bootcamp.template;

public interface Validator<C> {
	boolean isValid(C csvRecord);
}
