package com.worksap.bootcamp.template;


/**
 * It will return a group of Classes.
 * 
 * RecordReader#next() fetch each Record Entity. It means, RecordReader will have
 * a many DAOs. And this class will join each DTO and put other information into
 * Entity.<br>
 * 
 * GroupMaker#grouping(Record) grouping Record into GroupedRecord. It show Report's
 * one record.<br>
 * 
 * PageMaker#paging(Group) build up each Page. This class need to add Header and
 * Footer record.<br>
 * 
 * DocumentWriter#write(Page) will write each Page.<br>
 * 
 * GroupMaker and PageMaker need to keep some of Record or Group, then they have
 * a lastXxx method. When you reach to end of Record, you need to use these methods
 * to get last Groups or Pages.
 *
 * @param <R> type of Record(1 Record Entity)
 * @param <G> type of Group(1 Group Entity, Group will have a many Record)
 * @param <P> type of Page(1 Page Entity. Page will have header, footer, and Groups)
 */
public interface DocumentFactory<R, G, P> {
	DocumentWriter<P> getDocumentWriter();
	GroupMaker<R, G> getGroupMaker();
	PageMaker<G, P> getPageMaker();
	RecordReader<R> getRecordReader();
}
