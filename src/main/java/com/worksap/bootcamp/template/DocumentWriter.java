package com.worksap.bootcamp.template;

import java.io.IOException;

public interface DocumentWriter<P> extends AutoCloseable {
	void write(P page) throws IOException;

	/**
	 * If there are no data should be export, you should call this method. It
	 * will make special output for "NO DATA".
	 */
	void writeNoData() throws IOException;
}
