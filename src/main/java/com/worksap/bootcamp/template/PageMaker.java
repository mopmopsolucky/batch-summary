package com.worksap.bootcamp.template;

import java.util.List;

public interface PageMaker<G, P> {
	/**
	 * After all group put, you need to call this method and process last
	 * page(s).
	 * 
	 * @return
	 */
	List<P> lastPage();

	/**
	 * Make page from group.
	 * 
	 * @param group
	 * @return If group data enough to make page(s), return page(s). Or if not
	 *         yet enough, return empty list.
	 */
	List<P> paging(G group);
}
