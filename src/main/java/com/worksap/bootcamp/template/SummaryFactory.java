package com.worksap.bootcamp.template;

import java.io.IOException;

import com.worksap.bootcamp.io.Transaction;

public interface SummaryFactory<E, S, C> {
	RecordReader<E> getRecordReader();

	Summarizer<E, S> getSummarizer();

	SummaryWriter<S, C> getSummaryWriter();

	Transaction getTransaction() throws IOException;
}
