package com.worksap.bootcamp.template;

import java.io.IOException;

public interface SummaryWriter<S, C> extends AutoCloseable {
	boolean initialize(C condition) throws IOException;

	public void insert(S summary) throws IOException;
}
