package com.worksap.bootcamp.template;

import java.util.List;

public interface GroupMaker<R, G> {
	/**
	 * Make group about record.
	 * 
	 * @param record
	 * @return If record grouping key is break, return group(s). Or if key is
	 *         same, return empty list.
	 */
	List<G> grouping(R record);

	/**
	 * After all record put, you need to call this method and process last
	 * group(s).
	 * 
	 * @return
	 */
	List<G> lastGroup();
}
