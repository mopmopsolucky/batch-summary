package com.worksap.bootcamp.template;

public interface RecordReader<R> extends AutoCloseable {
	/**
	 * You should return next record or null(No data or end of data)
	 * 
	 * @return Record
	 */
	R next();
}
