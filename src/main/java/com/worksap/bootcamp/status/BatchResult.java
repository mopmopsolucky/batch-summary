package com.worksap.bootcamp.status;

public enum BatchResult {
	/**
	 * FAIL intvalue is defined by each product.
	 */
	FAIL(1), SUCCESS(0);

	private final int intValue;

	BatchResult(final int intValue) {
		this.intValue = intValue;
	}

	public final int intValue() {
		return this.intValue;
	}
}
