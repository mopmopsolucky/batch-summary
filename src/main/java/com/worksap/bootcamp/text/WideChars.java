package com.worksap.bootcamp.text;

import java.lang.Character.UnicodeBlock;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Support class to make "Text Report". It require to layout only use character
 * with mono space font. There is a type, "full width" and "half width"
 * characters. But no method and library support it. So I make it.
 * 
 * @author works
 */
public final class WideChars {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(WideChars.class);

	// I hope font has wide size glyph for these code block...
	static final Set<UnicodeBlock> SHOULD_HAS_FULL_WIDTH = Arrays
			.asList(UnicodeBlock.HIRAGANA, UnicodeBlock.KATAKANA,
					UnicodeBlock.CJK_COMPATIBILITY,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS,
					UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D,
					/* UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_E, */
					UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT)
			.stream().collect(Collectors.toSet());

	static final int UNKNOWN_SIZE = Integer.MIN_VALUE;

	private final EnumSet<Support> options;
	private final TargetCharset charset;
	private final TargetFont targetFont;

	public WideChars() {
		this(null, null, null);
	}

	public WideChars(final TargetCharset charset) {
		this(charset, null, null);
	}

	public WideChars(final TargetFont targetFont) {
		this(null, targetFont, null);
	}

	public WideChars(final TargetCharset charset, final TargetFont targetFont) {
		this(charset, targetFont, null);
	}

	public WideChars(final TargetCharset charset,
			final EnumSet<Support> supports) {
		this(charset, null, supports);
	}

	public WideChars(final TargetFont targetFont,
			final EnumSet<Support> supports) {
		this(null, targetFont, supports);
	}

	/**
	 * 
	 * @param charset
	 *            Default is UTF_8. If your out put Charset is decided, you
	 *            should set it.
	 * @param targetFont
	 *            If your out put Font is decided, you should set it.
	 * @param supports
	 *            You can set how to treat special characters. Such as
	 *            control(\\n).
	 */
	public WideChars(final TargetCharset charset, final TargetFont targetFont,
			final EnumSet<Support> supports) {
		if (charset == null) {
			this.charset = TargetCharset.UTF_8;
		} else {
			this.charset = charset;
		}

		this.targetFont = targetFont;

		if (supports == null) {
			this.options = EnumSet.noneOf(Support.class);
		} else {
			this.options = supports;
		}
	}

	/**
	 * Add (half) space to start of str.
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @return Space added String, or if str lengthW is larger than len return
	 *         Original String will return.
	 */
	public String leftPad(final String str, final int len) {
		return leftPad(str, len, ' ');
	}

	/**
	 * Add filler to start of str.
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @param filler
	 *            Any character you want to add to start of str.
	 * @return filler added String, or if str lengthW is larger than len return
	 *         Original String will return.
	 */
	public String leftPad(final String str, final int len, final char filler) {
		return fillString(str, Fill.LEFT, len, filler);
	}

	/**
	 * Count length with half(1)/full(2) width char.
	 * 
	 * @param str
	 *            Target String
	 * @return String length for old report
	 */
	public int length(final String str) {
		if (str == null) {
			return 0;
		}

		return str.codePoints().map(codePoint -> countCodePointWidth(codePoint))
				.sum();
	}

	/**
	 * Add (half) space to end of str.
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @return space added String, or if str lengthW is larger than len return
	 *         Original String will return.
	 */
	public String rightPad(final String str, final int len) {
		return rightPad(str, len, ' ');
	}

	/**
	 * Add filler to end of str.
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @param filler
	 *            Any character you want to add to end of str.
	 * @return filler added String, or if str lengthW is larger than len return
	 *         Original String will return.
	 */
	public String rightPad(final String str, final int len, final char filler) {
		return fillString(str, Fill.RIGHT, len, filler);
	}

	/**
	 * Trim str less than len (with half(1)/full(2) width).
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @return Trimed String, or if str {@link #length(String)} less than len
	 *         return Orignal String.
	 */
	public String trim(final String str, final int len) {
		return trim(str, len, null);
	}

	/**
	 * Trim str less than len (with half(1)/full(2) width).
	 * 
	 * @param str
	 *            Original String
	 * @param len
	 *            Length(with half(1)/full(2) width) of result String.
	 * @param filler
	 *            Width 1 character you want to add to end of str, if result
	 *            luck some length.
	 * @return Trimed String, or if str {@link #length(String)} less than len
	 *         return Orignal String.
	 */
	public String trim(final String str, final int len, final String filler) {
		if (length(str) < len) {
			return str;
		}

		// Null and Empty is OK
		if ((filler != null) && (filler.length() != 0)
				&& (length(filler) != 1)) {
			throw new IllegalArgumentException(String.format(
					"Filler should be half width character! %s width is %d",
					filler, length(filler)));
		}

		final StringBuilder result = new StringBuilder();

		str.codePoints().forEach(new IntConsumer() {
			int count = 0;

			@Override
			public void accept(final int codePoint) {
				int codePointSize = countCodePointWidth(codePoint);

				if (count <= len - codePointSize) {
					result.append(Character.toChars(codePoint));
					count += codePointSize;
				} else {
					while (count < len) {
						count++;

						if (filler != null) {
							result.append(filler);
						}
					}
				}
			}
		});

		return result.toString();
	}

	int countCodePointWidth(final int codePoint) {
		// return 1 if there is no possibility to use full width
		int result = checkOptions(codePoint);

		if (result != UNKNOWN_SIZE) {
			return result;
		}

		result = countWithCharset(codePoint);

		if (result != UNKNOWN_SIZE) {
			return result;
		}

		result = countWithFont(codePoint);

		if (result != UNKNOWN_SIZE) {
			return result;
		}

		return 1;
	}

	private int checkOptions(final int codePoint) {
		UnicodeBlock unicodeBlock = UnicodeBlock.of(codePoint);

		if (unicodeBlock == null) {
			return 0;
		}

		if (options.contains(Support.CONTROLS)) {
			/*
			 * U+0000-001F C0 controls U+007F DELETE U+0080-009F C1 controls
			 * 
			 * Think about Backspace(0x0008) and Delete(0x007F). It is better to
			 * return -1?
			 */
			if (unicodeBlock == UnicodeBlock.BASIC_LATIN) {
				if ((codePoint <= 0x1F) || (codePoint == 0x7F)) {
					return 0;
				}
			} else if (unicodeBlock == UnicodeBlock.LATIN_1_SUPPLEMENT) {
				if (codePoint >= 0x80 && codePoint <= 0x9F) {
					return 0;
				}
			}
		}

		if (options.contains(Support.DIACRITICAL_MARKS)) {
			/*
			 * This code point is additional information of previous code point.
			 * Then it has no width.
			 */
			if (unicodeBlock == UnicodeBlock.COMBINING_DIACRITICAL_MARKS) {
				return 0;
			}
		}

		if (options.contains(Support.IDC)) {
			/*
			 * IDC require 2 or 3 code point. And combine them into one
			 * Character. So, I need to return (accept code point count - 1) *
			 * -1.
			 */
			if (unicodeBlock == UnicodeBlock.IDEOGRAPHIC_DESCRIPTION_CHARACTERS) {
				if (codePoint == 0x2FF2 || codePoint == 0x2FF3) {
					// Accept 3 code points
					return -2;
				}

				return -1;
			}
		}

		if (options.contains(Support.VARIATION_SELECTORS)) {
			/*
			 * This code point is additional information of previous code point.
			 * Then it has no width.
			 */
			if ((unicodeBlock == UnicodeBlock.VARIATION_SELECTORS)
					|| (unicodeBlock == UnicodeBlock.VARIATION_SELECTORS_SUPPLEMENT)) {
				return 0;
			}
		}

		if (options.contains(Support.PRIVATE_USE_AREA_AS_FULL_WIDTH)) {
			if ((unicodeBlock == UnicodeBlock.PRIVATE_USE_AREA)
					|| (unicodeBlock == UnicodeBlock.SUPPLEMENTARY_PRIVATE_USE_AREA_A)
					|| (unicodeBlock == UnicodeBlock.SUPPLEMENTARY_PRIVATE_USE_AREA_B)) {
				return 2;
			}
		}

		return UNKNOWN_SIZE;
	}

	private int countWithCharset(final int codePoint) {
		if (!charset.canEncode(codePoint)) {
			return UNKNOWN_SIZE;
		}

		int result = charset.size(codePoint);

		if (result == UNKNOWN_SIZE) {
			LOGGER.trace("Character {}({}) is not supported by {}",
					String.valueOf(Character.toChars(codePoint)), codePoint,
					charset);
		}

		return result;
	}

	private int countWithFont(final int codePoint) {
		if (targetFont == null) {
			return UNKNOWN_SIZE;
		}

		int result = targetFont.size(codePoint);

		if (result == UNKNOWN_SIZE) {
			LOGGER.trace("Character {}({}) is not supported by {}",
					String.valueOf(Character.toChars(codePoint)), codePoint,
					targetFont);
		}

		return result;
	}

	/*
	 * Fill string with filler
	 */
	private String fillString(final String str, final Fill pos, final int len,
			final char filler) {
		final int strLen = length(str);

		if (strLen >= len) {
			return str;
		}

		final int incStep = countCodePointWidth(filler);

		if (incStep < 1)
			return str;

		StringBuilder result = new StringBuilder((str == null ? "" : str));

		for (int ii = strLen; ii <= (len - incStep); ii = ii + incStep) {
			if (pos == Fill.LEFT) {
				result.insert(0, filler);
			} else {
				result.append(filler);
			}
		}

		return result.toString();
	}
}
