package com.worksap.bootcamp.text;


public enum Support {
	CONTROLS,
	DIACRITICAL_MARKS,
	IDC,
	PRIVATE_USE_AREA_AS_FULL_WIDTH,
	VARIATION_SELECTORS,
}
