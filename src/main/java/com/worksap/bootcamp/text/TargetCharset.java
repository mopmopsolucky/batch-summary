package com.worksap.bootcamp.text;

import java.lang.Character.UnicodeBlock;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum TargetCharset {
	SJIS {
		private final CharsetEncoder encoder = Charset.forName("Shift_JIS")
				.newEncoder();

		@Override
		public boolean canEncode(final int codePoint) {
			return encoder
					.canEncode(String.valueOf(Character.toChars(codePoint)));
		}

		@Override
		public int size(final int codePoint) {
			ByteBuffer buffer = ByteBuffer.allocate(8);
			encoder.encode(CharBuffer.wrap(Character.toChars(codePoint)),
					buffer, true);
			encoder.flush(buffer);

			byte[] chars = buffer.array();

			if (chars[2] != 0) {
				return WideChars.UNKNOWN_SIZE;
			}

			if (((chars[0] >= (byte) 0x81) && (chars[0] <= (byte) 0x9f))
					|| ((chars[0] >= (byte) 0xE0)
							&& (chars[0] <= (byte) 0xFC))) {
				return 2;
			}

			return 1;
		}

	},
	UTF_8 {

		@Override
		public boolean canEncode(final int codePoint) {
			return Character.isValidCodePoint(codePoint);
		}

		@Override
		public int size(final int codePoint) {
			UnicodeBlock unicodeBlock = UnicodeBlock.of(codePoint);

			if (unicodeBlock == null) {
				return 0;
			}

			if (isNeedToPrintWideChar(codePoint, unicodeBlock)) {
				return WideChars.UNKNOWN_SIZE;
			}

			return 1;
		}

		boolean isNeedToPrintWideChar(final int codePoint,
				final UnicodeBlock unicodeBlock) {
			if (unicodeBlock == UnicodeBlock.LATIN_1_SUPPLEMENT) {
				// U+00A0-00BF Latin-1 punctuation and symbols
				if (codePoint >= 0xA0 && codePoint <= 0xBF) {
					return true;
				}

				// U+00D7 Multiplication Sign 乗算記号
				if (codePoint == 0xD7) {
					return true;
				}

				// U+00F7 Division Sign 除算記号
				if (codePoint == 0xF7) {
					return true;
				}

				return false;
			}

			if (unicodeBlock == UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
				/*
				 * U+FF01-FF5E Fullwidth ASCII variants U+FF5F-FF60 Fullwidth
				 * brackets
				 */
				if (codePoint >= 0xFF01 && codePoint <= 0xFF60) {
					return true;
				}

				if (codePoint >= 0xFFE0 && codePoint <= 0xFFE6) {
					return true;
				}
			}

			return NEED_TO_CHECK_FULL_WIDTH.contains(unicodeBlock);
		}

	};

	/*
	 * I want to use #addAll(WideChars.SHOULD_HAS_FULL_WIDTH). But #addAll
	 * return boolean...
	 */
	private static final Set<UnicodeBlock> NEED_TO_CHECK_FULL_WIDTH = Arrays
			.asList(UnicodeBlock.GREEK, UnicodeBlock.GENERAL_PUNCTUATION,
					UnicodeBlock.LETTERLIKE_SYMBOLS, UnicodeBlock.ARROWS,
					UnicodeBlock.NUMBER_FORMS,
					UnicodeBlock.MATHEMATICAL_OPERATORS,
					UnicodeBlock.MISCELLANEOUS_TECHNICAL,
					UnicodeBlock.BOX_DRAWING, UnicodeBlock.GEOMETRIC_SHAPES,
					UnicodeBlock.MISCELLANEOUS_SYMBOLS, UnicodeBlock.DINGBATS,
					UnicodeBlock.CJK_RADICALS_SUPPLEMENT,
					UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION,
					UnicodeBlock.HIRAGANA, UnicodeBlock.KATAKANA,
					UnicodeBlock.KATAKANA_PHONETIC_EXTENSIONS,
					UnicodeBlock.ENCLOSED_CJK_LETTERS_AND_MONTHS,
					UnicodeBlock.CJK_COMPATIBILITY,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS,
					UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C,
					UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D,
					/* UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_E, */
					UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT)
			.stream().collect(Collectors.toSet());

	public abstract boolean canEncode(final int codePoint);

	public abstract int size(final int codePoint);
}
